'use strict';

function resolveRemote(uri) {
  
    let split = (uri.href === undefined) ? uri.split(/[:\/]/).filter(e => e != '') : uri.href.toString().split(/[:\/]/).filter(e => e != '');
    let dataRemote = {};

    // ssh
    if (split.length === 3) {

        dataRemote = {
            protocol: 'ssh',
            host: split[0].replace(/(.*)@/g, ''),
            user: split[1],
            repo: split[2].replace('.git', ''),
        };

    } else if (split.length === 4) {
        // https

        dataRemote = {
            protocol: split[0].match(/http(s)/) ? split[0] : 'ssh',
            host: split[1].replace(/(.*)@/g, ''),
            user: split[2],
            repo: split[3].replace('.git', ''),
        };
    }
    
    return dataRemote;
}

function compare(x, y) {
    if (typeof x === 'string')
        x = resolveRemote(x);

    if (typeof y === 'string')
        y = resolveRemote(y);

    return (x.host === y.host &&
    x.user === y.user &&
    x.repo === y.repo);
}

function process(process) {
    const versioning = process.pm2_env.versioning;
    
    const app = {
        path: versioning.repo_path,
        git: _.merge({branch: versioning.branch}, resolveRemote(versioning.url)),
        pm2: {
            id: process.pm_id,
        },
    };

    return app;
}

function determineHost(body) {
    if (body.repository.url)
        return 'gitlab';

    return 'bitbucket';
}

function bitbucket(body) {
    return resolveRemote(body.repository.links.html);
}

function gitlab(body) {
    return resolveRemote(body.repository.url);
}

module.exports = {
    resolveRemote,
    compare,
    process,
    determineHost,
    bitbucket,
    gitlab,
};
