const Promise = require('bluebird');

const libs = [
  'pm2',
];

libs.forEach(lib => Promise.promisifyAll(require(lib)));
