Wimmersperg, the spielzeug app used mainly for specific triggers.

![](assets/wimmersperg.svg) 

It currently only has one function, is pulling changes whenever hook triggered. 

# Requirement

Wimmersperg is a specialized weapon. It only works at specific environment.

* global accessible `pm2` module
* `restify` as route handler, `lodash` and `moment` as utility

# Behaviour

It will pull the current branch, from `origin` remote URI.
If `origin` doesn't exist, it would fetch change from the first remote instead.

# Next

Even small weapon need improvement!

* web monitoring
* only pull tagged commit
